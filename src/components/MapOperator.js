function loadImage(platform) {
  if (platform === "Android") {
    loadImageFromDisk();
  } else {
    loadImageFromServer();
  }
}

function removeImage() {
  let sourceID = "base-image";
  let layerID = "base-image-layer";
  if (window.map.getLayer(layerID)) {
    window.map.removeLayer(layerID);
  }

  if (window.map.getSource(sourceID)) {
    window.map.removeSource(sourceID);
  }
}

function loadTerrain(platform) {
  if (platform === "Android") {
    loadTerrainFromDisk();
  } else {
    loadTerrainFromServer();
  }
}

function removeTerrain() {
  window.map.setTerrain();
  if (window.map.getSource("terrain")) {
    window.map.removeSource("terrain");
  }
}

function loadSky() {
  window.map.addLayer({
    id: "sky",
    type: "sky",
    paint: {
      // set up the sky layer to use a color gradient
      "sky-type": "gradient",
      // the sky will be lightest in the center and get darker moving radially outward
      // this simulates the look of the sun just below the horizon
      "sky-gradient": [
        "interpolate",
        ["linear"],
        ["sky-radial-progress"],
        0.8,
        "rgba(135, 206, 235, 1.0)",
        1,
        "rgba(0,0,0,0.1)",
      ],
      "sky-gradient-center": [0, 0],
      "sky-gradient-radius": 90,
      "sky-opacity": [
        "interpolate",
        ["exponential", 0.1],
        ["zoom"],
        5,
        0,
        22,
        1,
      ],
    },
  });
}

function removeSky() {
  window.map.removeLayer("sky");
}

function loadImageFromDisk() {
  window.map.addSource("base-image", {
    type: "mbtiles_raster",
    name: "image512.mbtiles",
    path: "/map/image512.mbtiles",
    tileSize: 512,
  });
  window.map.addLayer(
    {
      id: "base-image-layer",
      type: "raster",
      source: "base-image",
      layout: {},
      paint: {},
    },
    "china-boundary"
  );
}

function loadImageFromServer() {
  window.map.addSource("base-image", {
    type: "raster",
    tiles: [
      "http://192.168.5.162:8085/resource/mapData/map/image/{z}/{x}/{y}.webp",
    ],
    tileSize: 512,
  });
  window.map.addLayer(
    {
      id: "base-image-layer",
      type: "raster",
      source: "base-image",
      layout: {},
      paint: {},
    },
    "china-boundary"
  );
}

function loadTerrainFromDisk() {
  window.map.addSource("terrain", {
    type: "raster-dem",
    name: "terrain.mbtiles",
    path: "/map/terrain.mbtiles",
    tileSize: 512,
    maxzoom: 14,
  });

  window.map.setTerrain({ source: "terrain", exaggeration: 1.5 });
}

function loadTerrainFromServer() {}

export {
  loadImage,
  removeImage,
  loadTerrain,
  removeTerrain,
  loadSky,
  removeSky,
};
