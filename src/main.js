import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

let runningEnvironment = (function() {
	let domain = window.location.origin
	let path = window.location.pathname
	if (domain.indexOf('file') === 0 && path.indexOf('/android_asset/') === 0) {
		// 安卓手机模式
		return 'Android'
	}
	return 'Web'
})()
Vue.prototype.$re = runningEnvironment

if (runningEnvironment === 'Android') {
	document.addEventListener(
		'deviceready',
		function() {
			new Vue({
				router,
				store,
				render: h => h(App)
			}).$mount('#app')
		},
		false
	)
} else {
	new Vue({
		router,
		store,
		render: h => h(App)
	}).$mount('#app')
}
