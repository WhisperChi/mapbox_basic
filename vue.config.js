const path = require('path')

module.exports = {
	publicPath: './',
	chainWebpack: config => {
		if (process.env.use_analyzer) {
			// 分析
			config.plugin('webpack-bundle-analyzer').use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
		}
		if (process.env.NODE_ENV === 'test') {
			config.module
				.rule('istanbul')
				.test(/\.(js|vue)$/)
				.enforce('post')
				.include.add(path.resolve(__dirname, '/src'))
				.end()
				.use('istanbul-instrumenter-loader')
				.loader('istanbul-instrumenter-loader')
				.options({ esModules: true })
				.end()
		}
	},
	css: {
		loaderOptions: {
			less: {
				// 若 less-loader 版本小于 6.0，请移除 lessOptions 这一级，直接配置选项。
				lessOptions: {
					modifyVars: {
						hack: 'true;@import "src/style/vant/index.less";'
					}
				}
			}
		},
		sourceMap: true
	},
	configureWebpack: config => {
		//正式环境
		if (process.env.VUE_APP_MODE === 'prod') {
			console.log('当前的环境为生产环境')
			config.optimization = {
				splitChunks: {
					minSize: 20000, //生成块的最小大小
					maxSize: 25000, //将大于此数值的块拆分为较小的部分
					chunks: 'all'
				}
			}
			config.devtool = ''
		} else {
			console.log('当前的环境为开发测试环境')
			//test和dev环境都开启source map
			config.devtool = 'inline-source-map'
		}
	}
}

