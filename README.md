# mapbox_demo

## 简介

这是一个基于Vue+mapbox-gl.js的项目，用于作为基础功能，往上叠加功能，用来测试叠加的功能效率。

## 特性

1. 结合了tapd需求的gitlab源码功能，能够将需求对应到具体的代码
2. 结合了tapd缺陷的gitlab源码功能，能够将缺陷对应到具体的代码

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 演示
演示gitlab关联tapd需求id
